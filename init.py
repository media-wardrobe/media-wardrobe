#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Script for initialization of the database
"""

import os
import os.path
from os import path

import yaml

with open("config.yaml") as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    config_file = yaml.load(file, Loader=yaml.FullLoader)

print(config_file)

# Existieren DB user, serien und Fortschrittssammlung schon
if (path.exists(config_file['db-path'] + config_file['user-db-name']) and
    path.exists(config_file['db-path'] + config_file['progress-collection-db-name']) and
    path.exists(config_file['db-path'] + config_file['serien-db-name'])):
    print("Alle DBs existieren")
    
    number_files = len(os.listdir(config_file['db-update-path'])) # dir is your directory path
    # Ja - Prüfen ob Datei in Folder /web/database/update liegt
    if ( number_files > 0 ):
        print(number_files)

        # Ja - prüfen ob Dateiname eine neue Version der DB entspricht
            # DB Schema mittels des Update aktualisieren
    # Nein - Nutzer Hinweisen das DB existieren und keine aktualisierungen vorgenommen werden müssen
    else:
        print("Die Datenbanken sind eingerichtet und up to date")
# Nein - Prüfen ob ggf. eine der DBs existiert
else:
    print("Eine der DBs existiert nicht")
    # if (path.exists(config_file['db-path'] + config_file['user-db-name']):


    # Ja user DB existiert
        # prüfen ob DB Schema via update aktualisiert werden muss
        
    # Nein user DB existiert nicht
        # neue DB anlegen 
        
    # Ja serien DB existiert
        # prüfen ob DB Schema via update aktualisiert werden muss
        
    # Nein serien DB existiert nicht
        # neue DB anlegen 
        
    # Ja Fortschrittssammlung DB existiert
        # prüfen ob DB Schema via update aktualisiert werden muss
        
    # Nein Fortschrittssammlung DB existiert nicht
        # neue DB anlegen 
